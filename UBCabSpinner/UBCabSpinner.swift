//
//  UBCabSpinner.swift
//  UBCabSpinner
//
//  Created by Erdem Batsaikhan on 7/29/18.
//  Copyright © 2018 Erdem Batsaikhan. All rights reserved.
//

import UIKit

public enum UBCabSpinnerAnimationType : Int {
    case rotatingLogo = 0
    case circleScaleRippleMultiple = 1
    case rotatingTwoDots = 2
    case unknownAnimationType = 100
}

public protocol UBCabSpinnerAnimationProtocol: NSObjectProtocol {
    func setupAnimation(inLayer layer: CALayer, withSize size:CGSize, withTintColor color:UIColor)
}



public class UBCabSpinner: UIView {
    
    private var _animating: Bool = false
    private var _animationType: UBCabSpinnerAnimationType = UBCabSpinnerAnimationType.circleScaleRippleMultiple
    public var animationType: UBCabSpinnerAnimationType {
        set(newValue) {
            _animationType = newValue
            self.setupAnimation()
        }
        get { return _animationType }
    }
    
    private var _color: UIColor = UIColor.white
    public var color: UIColor{
        set(newValue){
            if !_color.isEqual(newValue) {
                _color = newValue
                let colorRef = _color.cgColor
                for sublayer in self.animationLayer.sublayers!{
                    sublayer.backgroundColor = colorRef
                    if sublayer is CAShapeLayer{
                        let shapeLayer = CAShapeLayer()
                        shapeLayer.strokeColor = colorRef
                        shapeLayer.fillColor = colorRef
                    }
                }
            }
        }
        get { return _color }
    }
    
    private var _defaultHeight: CGFloat = 50
    public var defaultHeight: CGFloat{
        set(newValue) {
            if _defaultHeight != newValue {
                _defaultHeight = newValue
                self.setupAnimation()
                self.invalidateIntrinsicContentSize()
            }
        }
        get { return _defaultHeight }
    }
    
    private var _defaultWidth: CGFloat = 50.0
    public var defaultWidth: CGFloat{
        set(newValue) {
            if _defaultWidth != newValue {
                _defaultWidth = newValue
                self.setupAnimation()
                self.invalidateIntrinsicContentSize()
            }
        }
        get { return _defaultWidth }
    }
    
    var animationLayer: CALayer! = CALayer()
    
    var logoName: String?
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public init(logoName: String) {
        self.logoName = logoName
        var logoFrame = CGRect(x: 0.0, y: 0.0, width: _defaultWidth, height: _defaultHeight)
        if let image = UIImage(named: self.logoName!){
            logoFrame.size.width = image.size.width
            logoFrame.size.height = image.size.height
        }
        super.init(frame: logoFrame)
        _animationType = .rotatingLogo
        self.commonInit()
    }
    
    public init(animationType type:UBCabSpinnerAnimationType){
        super.init(frame: CGRect(x: 0.0, y: 0.0, width: _defaultWidth, height: _defaultHeight))
        _animationType = type
        self.commonInit()
    }
    
    public init(tintColor color:UIColor){
        super.init(frame:CGRect(x: 0.0, y: 0.0, width: _defaultWidth, height: _defaultHeight))
        _color = color
        self.commonInit()
    }
    
    public init(animationType type:UBCabSpinnerAnimationType, tintColor color:UIColor){
        super.init(frame:CGRect(x: 0.0, y: 0.0, width: _defaultWidth, height: _defaultHeight))
        _animationType = type
        _color = color
        self.commonInit()
    }
    
    public init(animationType type:UBCabSpinnerAnimationType, tintColor color:UIColor, size:CGSize){
        super.init(frame:CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height))
        _defaultHeight = size.height
        _defaultWidth = size.width
        _animationType = type
        _color = color
        self.commonInit()
    }
    
    func commonInit(){
        self.isUserInteractionEnabled = false
        self.isHidden = true
        self.layer.addSublayer(self.animationLayer)
        self.setContentHuggingPriority(UILayoutPriority.required, for: NSLayoutConstraint.Axis.horizontal)
        self.setContentHuggingPriority(UILayoutPriority.required, for: NSLayoutConstraint.Axis.vertical)
    }
    
    func setupAnimation(){
        self.animationLayer.sublayers = nil
        let animation = spinnerAnimation(forAnimationType: _animationType)
        animation.setupAnimation(inLayer: self.animationLayer, withSize: CGSize(width: _defaultWidth, height: _defaultHeight), withTintColor: self.color)
        self.animationLayer.speed = 0.0
    }

    public func startAnimating(){
        if(self.animationLayer.sublayers == nil){
            self.setupAnimation()
        }
        self.isHidden = false
        self.animationLayer.speed = 1.0
        _animating = true
    }
    
    public func stopAnimating(){
        self.animationLayer.speed = 0.0
        self.animationLayer.removeAllAnimations()
        self.animationLayer.sublayers = nil
        _animating = false
        self.isHidden = true
    }
    
    

    public func spinnerAnimation(forAnimationType type:UBCabSpinnerAnimationType)->UBCabSpinnerAnimationProtocol{
        switch type {
        case .circleScaleRippleMultiple:
            return UBCabCircleRippleAnimation()
        case .rotatingLogo:
            if let logoName = self.logoName{
                return UBCabRotatingLogoAnimation(logo: logoName)
            }else{
                return UBCabCircleRippleAnimation()
            }
        default:
            return UBCabCircleRippleAnimation()
        }
    }
    
//    override public func layoutSubviews() {
//        super.layoutSubviews()
//        self.animationLayer.frame = self.bounds
//        let animating = _animating
//        if animating {
//            self.stopAnimating()
//        }
//        
//        self.setupAnimation()
//        
//        if animating {
//            self.startAnimating()
//        }
//        
//    }
    
    override public var intrinsicContentSize: CGSize {
        get {
            return CGSize(width: _defaultWidth, height: _defaultHeight)
        }
    }
}
