//
//  UBCabSpinnerAnimation.swift
//  UBCabSpinner
//
//  Created by Erdem Batsaikhan on 7/29/18.
//  Copyright © 2018 Erdem Batsaikhan. All rights reserved.
//

import UIKit

class UBCabSpinnerAnimation: NSObject, UBCabSpinnerAnimationProtocol {
    
    func createBasicAnimation(withKeyPath keyPath:String)->CABasicAnimation{
        let animation = CABasicAnimation(keyPath: keyPath)
        animation.isRemovedOnCompletion = false
        return animation
    }

    func createKeyFrameAnimation(withKeyPath keyPath:String)->CAKeyframeAnimation{
        let animation = CAKeyframeAnimation.init(keyPath: keyPath)
        animation.isRemovedOnCompletion = false
        return animation
    }
    
    func createAnimationGroup()->CAAnimationGroup{
        let animationGroup = CAAnimationGroup()
        animationGroup.isRemovedOnCompletion = false
        return animationGroup
    }
    
    func degreesToRadians(_ degrees:CGFloat) -> CGFloat {
        return degrees/180.0*CGFloat(Double.pi)
    }
    
    func setupAnimation(inLayer layer: CALayer, withSize size:CGSize, withTintColor color:UIColor){

    }
    
}
