//
//  UBCabCircleRippleAnimation.swift
//  UBCabSpinner
//
//  Created by Erdem Batsaikhan on 7/29/18.
//  Copyright © 2018 Erdem Batsaikhan. All rights reserved.
//

import UIKit

class UBCabCircleRippleAnimation: UBCabSpinnerAnimation {
    
    override func setupAnimation(inLayer layer: CALayer, withSize size: CGSize, withTintColor color: UIColor) {

        let duration = 1.25
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.21, 0.53, 0.56, 0.8)
        let timeOffsets = [0.0, 0.2, 0.4]
        
        let scaleAnimation = self.createKeyFrameAnimation(withKeyPath: "transform.scale")
        scaleAnimation.duration = duration
        scaleAnimation.keyTimes = [0.0, 0.7]
        scaleAnimation.values = [0.1, 1.0]
        scaleAnimation.timingFunction = timingFunction
        
        let opacityAnimation = self.createKeyFrameAnimation(withKeyPath: "opacity")
        opacityAnimation.duration = duration
        opacityAnimation.keyTimes = [0.0, 0.7, 1.0]
        opacityAnimation.values = [1.0, 0.7, 0.0]
        opacityAnimation.timingFunctions = [timingFunction, timingFunction]
        
        let animation = self.createAnimationGroup()
        animation.animations = [scaleAnimation, opacityAnimation]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.beginTime = CACurrentMediaTime()
        
        for i in 0..<3 {
            let circle = CAShapeLayer()
            let circlePath = UIBezierPath(roundedRect: CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height), cornerRadius: size.width/2.0)
            animation.timeOffset = timeOffsets[i]
            circle.fillColor = nil
            circle.lineWidth = 2.0
            circle.strokeColor = color.cgColor
            circle.path = circlePath.cgPath
            circle.add(animation, forKey: "animation")
            circle.frame = CGRect(x: (layer.bounds.size.width - size.width)/2.0, y: (layer.bounds.size.height - size.height)/2.0, width: size.width, height: size.height)
            layer.addSublayer(circle)
        }
    }
}
