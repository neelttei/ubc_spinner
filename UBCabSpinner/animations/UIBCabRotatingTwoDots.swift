//
//  UIBCabRotatingTwoDots.swift
//  UBCabSpinner
//
//  Created by Erdem Batsaikhan on 7/29/18.
//  Copyright © 2018 Erdem Batsaikhan. All rights reserved.
//

import UIKit

class UIBCabRotatingTwoDots: UBCabSpinnerAnimation {
    override func setupAnimation(inLayer layer: CALayer, withSize size: CGSize, withTintColor color: UIColor) {
        let beginTime = CACurrentMediaTime()
        
        let squareSize = floor(size.width/4.0)
        
        let oX = (layer.bounds.size.width - size.width)/2.0
        let oY = (layer.bounds.size.height - size.height)/2.0
        
        for i in 0..<2
        {
            let square:CALayer = CALayer()
            square.frame = CGRect(x: oX, y: oY, width: squareSize, height: squareSize)
            square.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            square.backgroundColor = color.cgColor
            square.shouldRasterize = true
            square.rasterizationScale = UIScreen.main.scale
            
            let transformAnimation = self.createKeyFrameAnimation(withKeyPath: "transform")
            transformAnimation.duration = 1.6
            transformAnimation.beginTime = beginTime - Double(i) * transformAnimation.duration/2.0
            transformAnimation.repeatCount = HUGE
            
            transformAnimation.keyTimes = [0.0, 0.25, 0.5, 0.75, 1.0]
            transformAnimation.timingFunctions = [
                CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut),
                CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut),
                CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut),
                CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut),
                CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut),
            ]
            
            var t1: CATransform3D = CATransform3DMakeTranslation(size.width - squareSize, 0.0, 0.0)
            t1 = CATransform3DRotate(t1, degreesToRadians(-90.0), 0.0, 0.0, 1.0)
            t1 = CATransform3DScale(t1, 0.5, 0.5, 1.0)
            
            var t2: CATransform3D = CATransform3DMakeTranslation(size.width - squareSize, size.height - squareSize, 0.0)
            t2 = CATransform3DRotate(t2, degreesToRadians(-180.0), 0.0, 0.0, 1.0)
            t2 = CATransform3DScale(t2, 1.0, 1.0, 1.0)
            
            var t3: CATransform3D = CATransform3DMakeTranslation(0.0, size.height - squareSize, 0.0)
            t3 = CATransform3DRotate(t3, degreesToRadians(-270.0), 0.0, 0.0, 1.0)
            t3 = CATransform3DScale(t3, 0.5, 0.5, 1.0)
            
            var t4: CATransform3D = CATransform3DMakeTranslation(0.0, 0.0, 0.0)
            t4 = CATransform3DRotate(t4, degreesToRadians(-360.0), 0.0, 0.0, 1.0)
            t4 = CATransform3DScale(t4, 1.0, 1.0, 1.0)
            
            transformAnimation.values = [
                NSValue.init(caTransform3D: CATransform3DIdentity),
                NSValue.init(caTransform3D: t1),
                NSValue.init(caTransform3D: t2),
                NSValue.init(caTransform3D: t3),
                NSValue.init(caTransform3D: t4),
            ]
            
            layer.addSublayer(square)
            square.add(transformAnimation, forKey: "animation")
            
        }
    }
}
