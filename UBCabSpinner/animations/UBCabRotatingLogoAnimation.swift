//
//  UBCabRotatingLogoAnimation.swift
//  UBCabSpinner
//
//  Created by Erdem Batsaikhan on 7/29/18.
//  Copyright © 2018 Erdem Batsaikhan. All rights reserved.
//

import UIKit

class UBCabRotatingLogoAnimation: UBCabSpinnerAnimation {
    
    var logo:String?

    public init(logo:String) {
        super.init()
        self.logo = logo
    }
    
    override func setupAnimation(inLayer layer: CALayer, withSize size: CGSize, withTintColor color: UIColor) {
        let duration = 3.0
        let timingFunction = CAMediaTimingFunction.init(controlPoints: 0.09, 0.57, 0.49, 0.9)
        
        let rotationXAnimation = self.createKeyFrameAnimation(withKeyPath: "transform.rotation.x")
        
        rotationXAnimation.duration = duration
        rotationXAnimation.keyTimes = [0.0, 0.25, 0.5, 0.75, 1.0]
        rotationXAnimation.values = [0.0, Double.pi, Double.pi, 0.0, 0.0]
        rotationXAnimation.timingFunctions = [timingFunction, timingFunction, timingFunction, timingFunction]
        
        let rotationYAnimation = self.createKeyFrameAnimation(withKeyPath: "transform.rotation.y")
        
        rotationYAnimation.duration = duration
        rotationYAnimation.keyTimes = [0.0, 0.25, 0.5, 0.75, 1.0]
        rotationYAnimation.values = [0.0, 0.0, Double.pi, Double.pi, 0.0]
        rotationYAnimation.timingFunctions = [timingFunction, timingFunction, timingFunction, timingFunction]
        
        let animation = self.createAnimationGroup()
        
        animation.animations = [rotationXAnimation, rotationYAnimation]
        animation.duration = duration
        animation.repeatCount = HUGE
        
        let imageLayer = CALayer()
        if let image = UIImage(named: self.logo!){
            imageLayer.frame = CGRect(x: 0.0, y: 0.0, width: image.size.width, height: image.size.height)
            imageLayer.contents = image.cgImage
        }
        imageLayer.add(animation, forKey: "animation")
        layer.addSublayer(imageLayer)
    }

}
